"use strict";
var router_1 = require('@angular/router');
var dashboard_component_1 = require('./components/dashboard/dashboard.component');
var airport_lists_component_1 = require("./components/airport-lists/airport-lists.component");
var flight_lists_component_1 = require("./components/flight-lists/flight-lists.component");
var service_selection_component_1 = require("./components/service-selection/service-selection.component");
var hotel_list_component_1 = require("./components/hotel-list/hotel-list.component");
var hotel_details_component_1 = require("./components/hotel-detail/hotel-details.component");
var page_not_found_component_1 = require("./components/page-not-found/page-not-found.component");
var transport_lists_component_1 = require("./components/transport-lists/transport-lists.component");
var appRoutes = [
    {
        path: '',
        redirectTo: '/home',
        pathMatch: 'full'
    },
    {
        path: 'home',
        component: dashboard_component_1.DashboardComponent,
    },
    {
        path: 'services',
        component: service_selection_component_1.ServiceSelectionComponent
    },
    {
        path: 'airports',
        component: airport_lists_component_1.AirportListsComponent
    },
    {
        path: 'flights',
        component: flight_lists_component_1.FlightListsComponent
    },
    {
        path: 'hotels',
        component: hotel_list_component_1.HotelListComponent
    },
    {
        path: 'hotels/details',
        component: hotel_details_component_1.HotelDetailsComponent
    },
    {
        path: 'transport',
        component: transport_lists_component_1.TransportListsComponent
    },
    {
        path: '**',
        component: page_not_found_component_1.PageNotFoundComponent
    }
];
exports.routing = router_1.RouterModule.forRoot(appRoutes);
exports.routedComponents = [dashboard_component_1.DashboardComponent];
//# sourceMappingURL=app.routing.js.map