export class Flight {
    origin: string;
    destination: string;
    departDate: string;
    arriveDate: string;
    airline: string;
    class: string
}
