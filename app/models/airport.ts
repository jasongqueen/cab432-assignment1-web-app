export class Airport {
    name: string;
    city: string;
    code: string;
    distance: number;
}
