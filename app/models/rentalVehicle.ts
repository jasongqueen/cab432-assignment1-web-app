export class RentalVehicle {
    vehicleType: string;
    transmission: string;
    category: string;
    currency: string;
    rateType: string;
    rate: string;
    total: string;
}
