export class Hotel {
    name: string;
    street: string;
    city: string;
    state: string;
    country: string;
    price: string
}
