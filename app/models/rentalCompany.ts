import {RentalVehicle} from "./rentalVehicle";

export class RentalCompany {
    name: string;
    street: string;
    city: string;
    state: string;
    country: string;
    vehicles: RentalVehicle[];
}
