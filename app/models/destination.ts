export class Destination {
    fullName: string;
    placeId: string;
    city: string;
    state: string;
    country: string;
    airportCode: string;
    lat: string;
    long: string;
}
