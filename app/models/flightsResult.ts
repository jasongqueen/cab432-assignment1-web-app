import {Itinerary} from "./flightItinerary";

export class FlightsResult {
    itineraries: Itinerary[];
    fare: number;
}
