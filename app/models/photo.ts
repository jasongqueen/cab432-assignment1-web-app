export class Photo {
    title: string;
    href: string;
    src: string;
}
