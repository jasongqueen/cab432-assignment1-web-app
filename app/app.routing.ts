import { Routes, RouterModule } from '@angular/router';

import { DashboardComponent } from './components/dashboard/dashboard.component';
import {AirportListsComponent} from "./components/airport-lists/airport-lists.component";
import {DataService} from "./services/data.service";
import {FlightListsComponent} from "./components/flight-lists/flight-lists.component";
import {ServiceSelectionComponent} from "./components/service-selection/service-selection.component";
import {HotelListComponent} from "./components/hotel-list/hotel-list.component";
import {HotelDetailsComponent} from "./components/hotel-detail/hotel-details.component";
import {PageNotFoundComponent} from "./components/page-not-found/page-not-found.component";
import {TransportListsComponent} from "./components/transport-lists/transport-lists.component";

const appRoutes: Routes = [
  {
    path: '',
    redirectTo: '/home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    component: DashboardComponent,
  },
  {
    path: 'services',
    component: ServiceSelectionComponent
  },
  {
    path: 'airports',
    component: AirportListsComponent
  },
  {
    path: 'flights',
    component: FlightListsComponent
  },
  {
    path: 'hotels',
    component: HotelListComponent
  },
  {
    path: 'hotels/details',
    component: HotelDetailsComponent
  },
  {
    path: 'transport',
    component: TransportListsComponent
  },
  {
    path: '**',
    component: PageNotFoundComponent
  }
];

export const routing = RouterModule.forRoot(appRoutes);

export const routedComponents = [DashboardComponent];
