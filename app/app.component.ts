import { Component } from '@angular/core';
import {ConfigService} from "./services/config.service";
import {Router} from "@angular/router";

@Component({
  moduleId: module.id,
  selector: 'my-app',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.css'],
  providers: [ConfigService]
})
export class AppComponent {
  title = 'LetsGoFindUs!';

  constructor(private router: Router) {
  }

}
