import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class ConfigService {

    private _config: Object;

    constructor(private http: Http) {
        this.load();
    }

    load() {
        return this.http
            .get("app/config/config.json")
            .map((r: Response) => r.json())
            .subscribe((data) => {
                            this._config = data;
                        });
    }

    get(key: any) {
        return this._config[key];
    }
}
