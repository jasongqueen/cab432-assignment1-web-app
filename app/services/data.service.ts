import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import {Destination} from "../models/destination";
import {ConfigService} from "./config.service";

import {Airport} from "../models/airport";
import {FlightsResult} from "../models/flightsResult";
import {Hotel} from "../models/hotel";
import {Photo} from "../models/photo";
import {RentalCompany} from "../models/rentalCompany";

@Injectable()
export class DataService {

    constructor(private http: Http,
                private configService: ConfigService) { }

    private _isReset: boolean = true;
    private _origin: Destination;
    private _originAirport: Airport;
    private _destination: Destination;
    private _destinationAirport: Airport;

    private _departDate: Date;
    private _returnDate: Date;

    private _hotel: Hotel;

    resetData(){
        this._isReset = true;
        this._origin = null;
        this._originAirport = null;
        this._destination = null;
        this._destinationAirport = null;
        this._departDate = null;
        this._returnDate = null;
    }

    searchDestination(term: string): Observable<Destination[]> {
        var apiUrl = this.configService.get('apiUrl');

        var result = this.http
            .get(apiUrl+`destinations/search/${term}`)
            // .get(`http://localhost:81/destinations/search/${term}`)
            .map((r: Response) => r.json() as Destination[]);

        return result;
    }

    getPlaceDetails(destination: Destination): Observable<Destination>{
        console.log(JSON.stringify(destination));
        var apiUrl = this.configService.get('apiUrl');
        var placeId = destination.placeId;

        var result = this.http
            .get(apiUrl+`destinations/details/`+ placeId)
            .map((r: Response) => destination = r.json() as Destination);
        return result;
    }

    getOriginDetails(){
        return this.getPlaceDetails(this._origin);
    }

    getDestinationDetails(){
        return this.getPlaceDetails(this._destination);
    }

    getAirports(destination: Destination): Observable<Airport[]>{
        var apiUrl = this.configService.get('apiUrl');
        var lat = destination.lat;
        var long = destination.long;

        var result = this.http
            .get(apiUrl+`destinations/airports/`+ lat + '/' + long)
            .map((r: Response) => r.json() as Airport[]);
        return result;
    }

    getOriginAirports(): Observable<Airport[]> {
        return this.getAirports(this._origin);
    }

    getDestinationAirports(): Observable<Airport[]> {
        return this.getAirports(this._destination);
    }

    getFlights(origin: Airport, destination: Airport, date :Date): Observable<FlightsResult[]> {
        var apiUrl = this.configService.get('apiUrl');

        var originCode = origin.code;
        var destinationCode = destination.code;
        var dateStr = date.getFullYear() + '-' + (date.getMonth()+1) + '-' + date.getDate();

        var result = this.http
            .get(apiUrl + 'flights/'+originCode+'/'+destinationCode+'/'+ dateStr)
            .map((r: Response) => r.json() as FlightsResult[]);

        return result;
    }

    getHotels(): Observable<Hotel[]>{
        var apiUrl = this.configService.get('apiUrl');
        var lat = this._destination.lat;
        var long = this._destination.long;
        var checkInDateStr = this._departDate.getFullYear() + '-' + (this._departDate.getMonth()+1) + '-' + this._departDate.getDate();
        var checkOutDateStr = this._returnDate.getFullYear() + '-' + (this._returnDate.getMonth()+1) + '-' + this._returnDate.getDate();

        var result = this.http
            .get(apiUrl+`hotels/`+ lat + '/' + long+ '/' + checkInDateStr+ '/' + checkOutDateStr)
            .map((r: Response) => r.json() as Hotel[]);
        return result;
    }

    getTransport(): Observable<RentalCompany[]>{
        var apiUrl = this.configService.get('apiUrl');
        var lat = this._destination.lat;
        var long = this._destination.long;
        var checkInDateStr = this._departDate.getFullYear() + '-' + (this._departDate.getMonth()+1) + '-' + this._departDate.getDate();
        var checkOutDateStr = this._returnDate.getFullYear() + '-' + (this._returnDate.getMonth()+1) + '-' + this._returnDate.getDate();

        var result = this.http
            .get(apiUrl+`transport/`+ lat + '/' + long+ '/' + checkInDateStr+ '/' + checkOutDateStr)
            .map((r: Response) => r.json() as RentalCompany[]);
        return result;
    }

    getHotelPhotos(): Observable<Photo[]>{
        var apiUrl = this.configService.get('apiUrl');
        var query = this._hotel.city + " " + this._hotel.country;

        var result = this.http
            .get(apiUrl+`photos/search/`+ query + '/5')
            .map((r: Response) => r.json() as Photo[]);
        return result;
    }

    getDepartFlights(): Observable<FlightsResult[]> {
        return this.getFlights(this._originAirport, this._destinationAirport, this._departDate);
    }

    getReturnFlights(): Observable<FlightsResult[]> {
        return this.getFlights(this._destinationAirport, this._originAirport, this._returnDate);
    }

    getOrigin(): Destination {
        return this._origin;
    }

    setOrigin(value: Destination) {
        this._origin = value;
    }

    getDestination(): Destination {
        return this._destination;
    }

    setDestination(value: Destination) {
        this._destination = value;
    }

    getDepartDate(): Date {
        return this._departDate;
    }

    setDepartDate(value: Date) {
        this._departDate = value;
    }

    getReturnDate(): Date {
        return this._returnDate;
    }

    setReturnDate(value: Date) {
        this._returnDate = value;
    }

    getOriginAirport(): Airport {
        return this._originAirport;
    }

    setOriginAirport(value: Airport) {
        this._originAirport = value;
    }

    getDestinationAirport(): Airport {
        return this._destinationAirport;
    }

    setDestinationAirport(value: Airport) {
        this._destinationAirport = value;
    }

    getIsReset(): boolean {
        return this._isReset;
    }

    setIsReset(value: boolean) {
        this._isReset = value;
    }


    getHotel(): Hotel {
        return this._hotel;
    }

    setHotel(value: Hotel) {
        this._hotel = value;
    }
}
