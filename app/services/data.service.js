"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
var config_service_1 = require("./config.service");
var DataService = (function () {
    function DataService(http, configService) {
        this.http = http;
        this.configService = configService;
        this._isReset = true;
    }
    DataService.prototype.resetData = function () {
        this._isReset = true;
        this._origin = null;
        this._originAirport = null;
        this._destination = null;
        this._destinationAirport = null;
        this._departDate = null;
        this._returnDate = null;
    };
    DataService.prototype.searchDestination = function (term) {
        var apiUrl = this.configService.get('apiUrl');
        var result = this.http
            .get(apiUrl + ("destinations/search/" + term))
            .map(function (r) { return r.json(); });
        return result;
    };
    DataService.prototype.getPlaceDetails = function (destination) {
        console.log(JSON.stringify(destination));
        var apiUrl = this.configService.get('apiUrl');
        var placeId = destination.placeId;
        var result = this.http
            .get(apiUrl + "destinations/details/" + placeId)
            .map(function (r) { return destination = r.json(); });
        return result;
    };
    DataService.prototype.getOriginDetails = function () {
        return this.getPlaceDetails(this._origin);
    };
    DataService.prototype.getDestinationDetails = function () {
        return this.getPlaceDetails(this._destination);
    };
    DataService.prototype.getAirports = function (destination) {
        var apiUrl = this.configService.get('apiUrl');
        var lat = destination.lat;
        var long = destination.long;
        var result = this.http
            .get(apiUrl + "destinations/airports/" + lat + '/' + long)
            .map(function (r) { return r.json(); });
        return result;
    };
    DataService.prototype.getOriginAirports = function () {
        return this.getAirports(this._origin);
    };
    DataService.prototype.getDestinationAirports = function () {
        return this.getAirports(this._destination);
    };
    DataService.prototype.getFlights = function (origin, destination, date) {
        var apiUrl = this.configService.get('apiUrl');
        var originCode = origin.code;
        var destinationCode = destination.code;
        var dateStr = date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate();
        var result = this.http
            .get(apiUrl + 'flights/' + originCode + '/' + destinationCode + '/' + dateStr)
            .map(function (r) { return r.json(); });
        return result;
    };
    DataService.prototype.getHotels = function () {
        var apiUrl = this.configService.get('apiUrl');
        var lat = this._destination.lat;
        var long = this._destination.long;
        var checkInDateStr = this._departDate.getFullYear() + '-' + (this._departDate.getMonth() + 1) + '-' + this._departDate.getDate();
        var checkOutDateStr = this._returnDate.getFullYear() + '-' + (this._returnDate.getMonth() + 1) + '-' + this._returnDate.getDate();
        var result = this.http
            .get(apiUrl + "hotels/" + lat + '/' + long + '/' + checkInDateStr + '/' + checkOutDateStr)
            .map(function (r) { return r.json(); });
        return result;
    };
    DataService.prototype.getTransport = function () {
        var apiUrl = this.configService.get('apiUrl');
        var lat = this._destination.lat;
        var long = this._destination.long;
        var checkInDateStr = this._departDate.getFullYear() + '-' + (this._departDate.getMonth() + 1) + '-' + this._departDate.getDate();
        var checkOutDateStr = this._returnDate.getFullYear() + '-' + (this._returnDate.getMonth() + 1) + '-' + this._returnDate.getDate();
        var result = this.http
            .get(apiUrl + "transport/" + lat + '/' + long + '/' + checkInDateStr + '/' + checkOutDateStr)
            .map(function (r) { return r.json(); });
        return result;
    };
    DataService.prototype.getHotelPhotos = function () {
        var apiUrl = this.configService.get('apiUrl');
        var query = this._hotel.city + " " + this._hotel.country;
        var result = this.http
            .get(apiUrl + "photos/search/" + query + '/5')
            .map(function (r) { return r.json(); });
        return result;
    };
    DataService.prototype.getDepartFlights = function () {
        return this.getFlights(this._originAirport, this._destinationAirport, this._departDate);
    };
    DataService.prototype.getReturnFlights = function () {
        return this.getFlights(this._destinationAirport, this._originAirport, this._returnDate);
    };
    DataService.prototype.getOrigin = function () {
        return this._origin;
    };
    DataService.prototype.setOrigin = function (value) {
        this._origin = value;
    };
    DataService.prototype.getDestination = function () {
        return this._destination;
    };
    DataService.prototype.setDestination = function (value) {
        this._destination = value;
    };
    DataService.prototype.getDepartDate = function () {
        return this._departDate;
    };
    DataService.prototype.setDepartDate = function (value) {
        this._departDate = value;
    };
    DataService.prototype.getReturnDate = function () {
        return this._returnDate;
    };
    DataService.prototype.setReturnDate = function (value) {
        this._returnDate = value;
    };
    DataService.prototype.getOriginAirport = function () {
        return this._originAirport;
    };
    DataService.prototype.setOriginAirport = function (value) {
        this._originAirport = value;
    };
    DataService.prototype.getDestinationAirport = function () {
        return this._destinationAirport;
    };
    DataService.prototype.setDestinationAirport = function (value) {
        this._destinationAirport = value;
    };
    DataService.prototype.getIsReset = function () {
        return this._isReset;
    };
    DataService.prototype.setIsReset = function (value) {
        this._isReset = value;
    };
    DataService.prototype.getHotel = function () {
        return this._hotel;
    };
    DataService.prototype.setHotel = function (value) {
        this._hotel = value;
    };
    DataService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http, config_service_1.ConfigService])
    ], DataService);
    return DataService;
}());
exports.DataService = DataService;
//# sourceMappingURL=data.service.js.map