"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var platform_browser_1 = require('@angular/platform-browser');
var forms_1 = require('@angular/forms');
var http_1 = require('@angular/http');
require('./extentions/rxjs-extensions');
var app_component_1 = require('./app.component');
var app_routing_1 = require('./app.routing');
var dashboard_component_1 = require("./components/dashboard/dashboard.component");
var banner_component_1 = require("./components/banner/banner.component");
var config_service_1 = require("./services/config.service");
var datepicker_component_1 = require("./components/datepicker/datepicker.component");
var ng2_bootstrap_1 = require('ng2-bootstrap/ng2-bootstrap');
var destination_search_component_1 = require("./components/destination-search/destination-search.component");
var data_service_1 = require("./services/data.service");
var airport_lists_component_1 = require("./components/airport-lists/airport-lists.component");
var flight_lists_component_1 = require("./components/flight-lists/flight-lists.component");
var service_selection_component_1 = require("./components/service-selection/service-selection.component");
var hotel_list_component_1 = require("./components/hotel-list/hotel-list.component");
var hotel_details_component_1 = require("./components/hotel-detail/hotel-details.component");
var page_not_found_component_1 = require("./components/page-not-found/page-not-found.component");
var transport_lists_component_1 = require("./components/transport-lists/transport-lists.component");
var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            imports: [
                platform_browser_1.BrowserModule,
                forms_1.FormsModule,
                app_routing_1.routing,
                http_1.HttpModule,
                ng2_bootstrap_1.Ng2BootstrapModule
            ],
            declarations: [
                app_component_1.AppComponent,
                app_routing_1.routedComponents,
                banner_component_1.BannerComponent,
                dashboard_component_1.DashboardComponent,
                datepicker_component_1.DatepickerComponent,
                destination_search_component_1.DestinationSearchComponent,
                airport_lists_component_1.AirportListsComponent,
                flight_lists_component_1.FlightListsComponent,
                service_selection_component_1.ServiceSelectionComponent,
                hotel_list_component_1.HotelListComponent,
                hotel_details_component_1.HotelDetailsComponent,
                transport_lists_component_1.TransportListsComponent,
                page_not_found_component_1.PageNotFoundComponent
            ],
            providers: [
                config_service_1.ConfigService,
                data_service_1.DataService
            ],
            bootstrap: [app_component_1.AppComponent]
        }), 
        __metadata('design:paramtypes', [])
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map