import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import './extentions/rxjs-extensions';
import { AppComponent } from './app.component';
import { routing, routedComponents } from './app.routing';

import {DashboardComponent} from "./components/dashboard/dashboard.component";
import {BannerComponent} from "./components/banner/banner.component";

import {ConfigService} from "./services/config.service";
import {DatepickerComponent} from "./components/datepicker/datepicker.component";
import { Ng2BootstrapModule } from 'ng2-bootstrap/ng2-bootstrap';
import {DestinationSearchComponent} from "./components/destination-search/destination-search.component";
import {DataService} from "./services/data.service";
import {AirportListsComponent} from "./components/airport-lists/airport-lists.component";
import {FlightListsComponent} from "./components/flight-lists/flight-lists.component";
import {ServiceSelectionComponent} from "./components/service-selection/service-selection.component";
import {HotelListComponent} from "./components/hotel-list/hotel-list.component";
import {HotelDetailsComponent} from "./components/hotel-detail/hotel-details.component";
import {PageNotFoundComponent} from "./components/page-not-found/page-not-found.component";
import {TransportListsComponent} from "./components/transport-lists/transport-lists.component";

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    routing,
    HttpModule,
    Ng2BootstrapModule
  ],
  declarations: [
    AppComponent,
    routedComponents,
    BannerComponent,
    DashboardComponent,
    DatepickerComponent,
    DestinationSearchComponent,
    AirportListsComponent,
    FlightListsComponent,
    ServiceSelectionComponent,
      HotelListComponent,
    HotelDetailsComponent,
      TransportListsComponent,
      PageNotFoundComponent
  ],
  providers: [
      ConfigService,
      DataService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
