"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var data_service_1 = require("../../services/data.service");
var AirportListsComponent = (function () {
    function AirportListsComponent(router, dataService) {
        this.router = router;
        this.dataService = dataService;
        this.isOriginAirportSelected = false;
        this.isDestinationAirportSelected = false;
    }
    AirportListsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.dataService.getOriginDetails()
            .subscribe(function (res) {
            _this.dataService.setOrigin(res);
            _this.loadOriginAirportList();
        }, function (err) {
            console.log("Error when retrieving origin airports");
        });
        this.dataService.getDestinationDetails()
            .subscribe(function (res) {
            _this.dataService.setDestination(res);
            _this.loadDestinationAirportList();
        }, function (err) {
            console.log("Error when retrieving destination airports");
        });
    };
    AirportListsComponent.prototype.loadOriginAirportList = function () {
        var _this = this;
        this.dataService.getOriginAirports()
            .subscribe(function (res) {
            _this.originAirports = res;
            if (_this.originAirports == null) {
                alert("No airports were found");
            }
        }, function (err) {
            console.log("Error when retrieving origin airports");
        });
    };
    AirportListsComponent.prototype.loadDestinationAirportList = function () {
        var _this = this;
        this.dataService.getDestinationAirports()
            .subscribe(function (res) {
            _this.destinationAirports = res;
            if (_this.destinationAirports == null) {
                alert("No airports were found");
            }
        }, function (err) {
            console.log("Error when retrieving destination airports");
        });
    };
    AirportListsComponent.prototype.selectOriginAirport = function (airport) {
        this.isOriginAirportSelected = true;
        this.dataService.setOriginAirport(airport);
    };
    AirportListsComponent.prototype.selectDestinationAirport = function (airport) {
        this.isDestinationAirportSelected = true;
        this.dataService.setDestinationAirport(airport);
        this.checkAirports();
    };
    AirportListsComponent.prototype.checkAirports = function () {
        if (this.isDestinationAirportSelected != true && this.isOriginAirportSelected != true) {
            alert("Something went wrong. Try searching again.");
            this.router.navigate(['/home']);
        }
        this.router.navigate(['/flights']);
    };
    AirportListsComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'my-airport-lists',
            templateUrl: 'airport-lists.component.html'
        }), 
        __metadata('design:paramtypes', [router_1.Router, data_service_1.DataService])
    ], AirportListsComponent);
    return AirportListsComponent;
}());
exports.AirportListsComponent = AirportListsComponent;
//# sourceMappingURL=airport-lists.component.js.map