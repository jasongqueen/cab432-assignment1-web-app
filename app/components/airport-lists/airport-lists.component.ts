import {Component, OnInit, EventEmitter, Output} from '@angular/core';
import { Router } from '@angular/router';

import {DataService} from "../../services/data.service";
import {Airport} from "../../models/airport";

@Component({
  moduleId: module.id,
  selector: 'my-airport-lists',
  templateUrl: 'airport-lists.component.html'
})
export class AirportListsComponent implements OnInit {

    private originAirports: Airport[];
    private destinationAirports: Airport[];

    private isOriginAirportSelected: boolean = false;
    private isDestinationAirportSelected: boolean = false;

    constructor(private router: Router, private dataService: DataService) {
    }

    ngOnInit(): void {
      this.dataService.getOriginDetails()
          .subscribe(
              res => {
                  this.dataService.setOrigin(res);
                  this.loadOriginAirportList();
              },
              err => {
                  console.log("Error when retrieving origin airports")
              }
          );

      this.dataService.getDestinationDetails()
          .subscribe(
              res => {
                  this.dataService.setDestination(res);
                  this.loadDestinationAirportList();
              },
              err => {
                  console.log("Error when retrieving destination airports")
              }
          );
    }

    loadOriginAirportList(){
      this.dataService.getOriginAirports()
          .subscribe(
              res => {
                  this.originAirports = res;
                  if(this.originAirports == null){
                      alert("No airports were found")
                  }
              },
              err => {
                  console.log("Error when retrieving origin airports")
              }
          );
    }

    loadDestinationAirportList(){
        this.dataService.getDestinationAirports()
            .subscribe(
                res => {
                    this.destinationAirports = res;
                    if(this.destinationAirports == null){
                       alert("No airports were found")
                    }
                },
                err => {
                    console.log("Error when retrieving destination airports")
                }
            );
    }

    selectOriginAirport(airport: Airport){
        this.isOriginAirportSelected = true;
        this.dataService.setOriginAirport(airport);
    }

    selectDestinationAirport(airport: Airport){
        this.isDestinationAirportSelected = true;
        this.dataService.setDestinationAirport(airport);
        this.checkAirports();
    }

    checkAirports(){
        if(this.isDestinationAirportSelected != true && this.isOriginAirportSelected != true){
            alert("Something went wrong. Try searching again.");
            this.router.navigate(['/home']);
        }
        this.router.navigate(['/flights']);
    }


}
