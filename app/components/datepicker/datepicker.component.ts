import {Component, Input, Output, EventEmitter} from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'my-datepicker',
    templateUrl: 'datepicker.component.html'
})
export class DatepickerComponent {
    @Input()
    dateModel: Date;
    @Input()
    label: string;
    @Output()
    dateChangeEvent: EventEmitter<Date> = new EventEmitter<Date>();
    public minDate:Date = void 0;
    private showDatepicker: boolean = false;

    showPopup() {
        this.showDatepicker = true;
    }

    hidePopup(event: Date) {
        this.showDatepicker = false;
        this.dateModel = event;
        this.dateChangeEvent.emit(event)
    }
}