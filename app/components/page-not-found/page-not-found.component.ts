import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute, NavigationEnd} from '@angular/router';
import {Observable} from "rxjs/observable";
import {DataService} from "../../services/data.service";


@Component({
    moduleId: module.id,
    selector: 'my-not-found',
    templateUrl: 'page-not-found.component.html',
    styleUrls: ['page-not-found.component.css']
})
export class PageNotFoundComponent implements OnInit {
    token: Observable<string>;
    title = 'LetsGoFindMe!';

    constructor(
        private router: Router) {
    }

    ngOnInit(): void {
    }
}
