import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute, NavigationEnd} from '@angular/router';
import {Observable} from "rxjs/observable";
import {DataService} from "../../services/data.service";


@Component({
  moduleId: module.id,
  selector: 'my-service-selection',
  templateUrl: 'service-selection.component.html'
})
export class ServiceSelectionComponent implements OnInit {
  token: Observable<string>;
  title = 'LetsGoFindMe!';

  constructor(
    private router: Router, private dataService: DataService) {
  }

  ngOnInit(): void {
    this.dataService.getOriginDetails()
        .subscribe(
            res => {
              this.dataService.setOrigin(res);
            },
            err => {
              console.log("Error when retrieving origin details")
            }
        );

    this.dataService.getDestinationDetails()
        .subscribe(
            res => {
              this.dataService.setDestination(res);
            },
            err => {
              console.log("Error when retrieving destination details")
            }
        );
  }
}
