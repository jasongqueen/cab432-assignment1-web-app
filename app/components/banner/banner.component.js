"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var destination_1 = require("../../models/destination");
var data_service_1 = require("../../services/data.service");
var BannerComponent = (function () {
    function BannerComponent(router, dataService) {
        this.router = router;
        this.dataService = dataService;
        this.fromPlace = new destination_1.Destination();
        this.toPlace = new destination_1.Destination();
    }
    BannerComponent.prototype.ngOnInit = function () {
    };
    BannerComponent.prototype.search = function () {
        console.log("departDate: " + JSON.stringify(this.departDate));
        console.log("returnDate: " + JSON.stringify(this.returnDate));
        console.log("toPlace: " + JSON.stringify(this.toPlace));
        console.log("fromPlace: " + JSON.stringify(this.fromPlace));
        // Check departure place
        if (this.fromPlace.fullName == null || typeof this.fromPlace.fullName === 'undefined') {
            alert("Please select a valid place of departure");
            return;
        }
        // Check to place
        if (this.toPlace.fullName == null || typeof this.toPlace.fullName === 'undefined') {
            alert("Please select a valid destination");
            return;
        }
        // Check departure date
        if (this.departDate == null || typeof this.departDate === 'undefined') {
            alert("Please select a return date");
            return;
        }
        // Check return date
        if (this.returnDate == null || typeof this.returnDate === 'undefined') {
            alert("Please select a return date");
            return;
        }
        // Check dates are valid
        if ((this.returnDate.getTime() - this.departDate.getTime()) < 0) {
            alert("Please select a return date which is after the depart date");
            return;
        }
        // Check dates are valid
        if ((this.returnDate.getTime() - new Date().getTime()) < 0) {
            alert("Please select a return date which is after the current date");
            return;
        }
        // Check dates are valid
        if ((this.departDate.getTime() - new Date().getTime()) < 0) {
            alert("Please select a depart date which is after the current date");
            return;
        }
        this.dataService.resetData();
        this.dataService.setOrigin(this.fromPlace);
        this.dataService.setDestination(this.toPlace);
        this.dataService.setReturnDate(this.returnDate);
        this.dataService.setDepartDate(this.departDate);
        this.router.navigate(["../services"], { fragment: 'services' });
    };
    BannerComponent.prototype.fromPlaceEvent = function (place) {
        this.fromPlace = place;
    };
    BannerComponent.prototype.toPlaceEvent = function (place) {
        this.toPlace = place;
    };
    BannerComponent.prototype.returnDateEvent = function (date) {
        this.returnDate = date;
    };
    BannerComponent.prototype.departDateEvent = function (date) {
        this.departDate = date;
    };
    BannerComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'my-banner',
            templateUrl: 'banner.component.html'
        }), 
        __metadata('design:paramtypes', [router_1.Router, data_service_1.DataService])
    ], BannerComponent);
    return BannerComponent;
}());
exports.BannerComponent = BannerComponent;
//# sourceMappingURL=banner.component.js.map