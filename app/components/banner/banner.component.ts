import {Component, OnInit, EventEmitter, Output} from '@angular/core';
import { Router } from '@angular/router';

import {Destination} from "../../models/destination";
import {DataService} from "../../services/data.service";

@Component({
  moduleId: module.id,
  selector: 'my-banner',
  templateUrl: 'banner.component.html'
})
export class BannerComponent implements OnInit {

  private fromPlace: Destination = new Destination();
  private toPlace: Destination =  new Destination();

  private departDate: Date;
  private returnDate: Date;

  constructor(private router: Router, private dataService: DataService) {
  }

  ngOnInit(): void {
  }

  search(){
    console.log("departDate: "+JSON.stringify(this.departDate));
    console.log("returnDate: "+JSON.stringify(this.returnDate));
    console.log("toPlace: "+JSON.stringify(this.toPlace));
    console.log("fromPlace: "+JSON.stringify(this.fromPlace));

    // Check departure place
    if(this.fromPlace.fullName == null || typeof this.fromPlace.fullName === 'undefined'){
      alert("Please select a valid place of departure");
      return;
    }
    // Check to place
    if(this.toPlace.fullName == null || typeof this.toPlace.fullName === 'undefined'){
      alert("Please select a valid destination");
      return;
    }
    // Check departure date
    if(this.departDate == null || typeof this.departDate === 'undefined'){
      alert("Please select a return date");
      return;
    }
    // Check return date
    if(this.returnDate == null || typeof this.returnDate === 'undefined'){
      alert("Please select a return date");
      return;
    }
    // Check dates are valid
    if ((this.returnDate.getTime() - this.departDate.getTime()) < 0){
      alert("Please select a return date which is after the depart date");
      return;
    }
    // Check dates are valid
    if ((this.returnDate.getTime() - new Date().getTime()) < 0){
      alert("Please select a return date which is after the current date");
      return;
    }
    // Check dates are valid
    if ((this.departDate.getTime() - new Date().getTime()) < 0){
      alert("Please select a depart date which is after the current date");
      return;
    }

    this.dataService.resetData();

    this.dataService.setOrigin(this.fromPlace);
    this.dataService.setDestination(this.toPlace);
    this.dataService.setReturnDate(this.returnDate);
    this.dataService.setDepartDate(this.departDate);

    this.router.navigate(["../services"], { fragment: 'services' });
  }

  fromPlaceEvent(place: Destination){
    this.fromPlace = place;
  }
  toPlaceEvent(place: Destination){
    this.toPlace = place;
  }
  returnDateEvent(date: Date){
    this.returnDate = date;
  }
  departDateEvent(date: Date){
    this.departDate = date;
  }

}
