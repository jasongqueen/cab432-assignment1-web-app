import {Component, OnInit, EventEmitter, Output} from '@angular/core';
import { Router } from '@angular/router';

import {DataService} from "../../services/data.service";
import {Airport} from "../../models/airport";
import {Hotel} from "../../models/hotel";

@Component({
  moduleId: module.id,
  selector: 'my-hotel-list',
  templateUrl: 'hotel-list.component.html'
})
export class HotelListComponent implements OnInit {

    private hotels: Hotel[];

    constructor(private router: Router, private dataService: DataService) {
    }

    ngOnInit(): void {
      this.dataService.getHotels()
          .subscribe(
              res => {
                  this.hotels = res;
              },
              err => {
                  console.log("Error when retrieving hotels");
              }
          );
    }

    selecthotel(hotel: Hotel){
        this.dataService.setHotel(hotel);
        this.router.navigate(["../hotels/details"]);
    }


}
