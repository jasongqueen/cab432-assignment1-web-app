"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var data_service_1 = require("../../services/data.service");
var HotelListComponent = (function () {
    function HotelListComponent(router, dataService) {
        this.router = router;
        this.dataService = dataService;
    }
    HotelListComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.dataService.getHotels()
            .subscribe(function (res) {
            _this.hotels = res;
        }, function (err) {
            console.log("Error when retrieving hotels");
        });
    };
    HotelListComponent.prototype.selecthotel = function (hotel) {
        this.dataService.setHotel(hotel);
        this.router.navigate(["../hotels/details"]);
    };
    HotelListComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'my-hotel-list',
            templateUrl: 'hotel-list.component.html'
        }), 
        __metadata('design:paramtypes', [router_1.Router, data_service_1.DataService])
    ], HotelListComponent);
    return HotelListComponent;
}());
exports.HotelListComponent = HotelListComponent;
//# sourceMappingURL=hotel-list.component.js.map