import {Component, OnInit, EventEmitter, Output} from '@angular/core';
import { Router } from '@angular/router';

import {DataService} from "../../services/data.service";
import {FlightsResult} from "../../models/flightsResult";
import {RentalCompany} from "../../models/rentalCompany";

@Component({
  moduleId: module.id,
  selector: 'my-transport-lists',
  templateUrl: 'transport-lists.component.html'
})
export class TransportListsComponent implements OnInit {

    private rentalCompanies: RentalCompany[];

    constructor(private router: Router, private dataService: DataService) {
    }

    ngOnInit(): void {
        this.dataService.getTransport()
            .subscribe(
                res => {
                    this.rentalCompanies = res;
                    if(this.rentalCompanies == null){
                        alert("No rental vehicles were found")
                    }
                },
                err => {
                    console.log("Error when retrieving rental vehicles")
                }
            );
    }


}
