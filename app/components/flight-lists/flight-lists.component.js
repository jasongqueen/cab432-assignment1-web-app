"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var data_service_1 = require("../../services/data.service");
var FlightListsComponent = (function () {
    function FlightListsComponent(router, dataService) {
        this.router = router;
        this.dataService = dataService;
        this.hideDeparting = false;
        this.hideReturning = true;
    }
    FlightListsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.dataService.getDepartFlights()
            .subscribe(function (res) {
            _this.departFlights = res;
            if (_this.departFlights == null) {
                alert("No airports were found");
            }
        }, function (err) {
            console.log("Error when retrieving departing flights");
        });
        this.dataService.getReturnFlights()
            .subscribe(function (res) {
            _this.returnFlights = res;
            if (_this.returnFlights == null) {
                alert("No airports were found");
            }
        }, function (err) {
            console.log("Error when retrieving returning flights");
        });
    };
    FlightListsComponent.prototype.switchFlights = function () {
        this.hideDeparting = !this.hideDeparting;
        this.hideReturning = !this.hideReturning;
    };
    FlightListsComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'my-flight-lists',
            templateUrl: 'flight-lists.component.html'
        }), 
        __metadata('design:paramtypes', [router_1.Router, data_service_1.DataService])
    ], FlightListsComponent);
    return FlightListsComponent;
}());
exports.FlightListsComponent = FlightListsComponent;
//# sourceMappingURL=flight-lists.component.js.map