import {Component, OnInit, EventEmitter, Output} from '@angular/core';
import { Router } from '@angular/router';

import {DataService} from "../../services/data.service";
import {FlightsResult} from "../../models/flightsResult";

@Component({
  moduleId: module.id,
  selector: 'my-flight-lists',
  templateUrl: 'flight-lists.component.html'
})
export class FlightListsComponent implements OnInit {

    private departFlights: FlightsResult[];
    private returnFlights: FlightsResult[];

    private hideDeparting:boolean = false;
    private hideReturning:boolean = true;

    constructor(private router: Router, private dataService: DataService) {
    }

    ngOnInit(): void {
        this.dataService.getDepartFlights()
            .subscribe(
                res => {
                    this.departFlights = res;
                    if(this.departFlights == null){
                        alert("No airports were found")
                    }
                },
                err => {
                    console.log("Error when retrieving departing flights")
                }
            );

        this.dataService.getReturnFlights()
            .subscribe(
                res => {
                    this.returnFlights = res;
                    if(this.returnFlights == null){
                        alert("No airports were found")
                    }
                },
                err => {
                    console.log("Error when retrieving returning flights")
                }
            );
    }

    switchFlights(){
        this.hideDeparting = !this.hideDeparting;
        this.hideReturning = !this.hideReturning;
    }

}
