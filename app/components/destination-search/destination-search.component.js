"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var Observable_1 = require('rxjs/Observable');
var Subject_1 = require('rxjs/Subject');
var destination_1 = require("../../models/destination");
var router_1 = require("@angular/router");
var data_service_1 = require("../../services/data.service");
var DestinationSearchComponent = (function () {
    function DestinationSearchComponent(dataService, router) {
        this.dataService = dataService;
        this.router = router;
        this.placeChangeEvent = new core_1.EventEmitter();
        this.searchTerms = new Subject_1.Subject();
    }
    DestinationSearchComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.places = this.searchTerms
            .debounceTime(300) // wait for 300ms pause in events
            .distinctUntilChanged() // ignore if next search term is same as previous
            .switchMap(function (term) { return term // switch to new observable each time
            ? _this.dataService.searchDestination(term)
            : Observable_1.Observable.of([]); });
    };
    DestinationSearchComponent.prototype.search = function (term) {
        this.searchTerms.next(term);
    };
    DestinationSearchComponent.prototype.selectedDestinationEvent = function (event) {
        console.log();
        this.placeModel = event;
        this.placeChangeEvent.emit(event);
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', destination_1.Destination)
    ], DestinationSearchComponent.prototype, "placeModel", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', String)
    ], DestinationSearchComponent.prototype, "label", void 0);
    __decorate([
        core_1.Output(), 
        __metadata('design:type', core_1.EventEmitter)
    ], DestinationSearchComponent.prototype, "placeChangeEvent", void 0);
    DestinationSearchComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'my-destination-search',
            templateUrl: 'destination-search.component.html',
            providers: [data_service_1.DataService]
        }), 
        __metadata('design:paramtypes', [data_service_1.DataService, router_1.Router])
    ], DestinationSearchComponent);
    return DestinationSearchComponent;
}());
exports.DestinationSearchComponent = DestinationSearchComponent;
//# sourceMappingURL=destination-search.component.js.map