import {Component, Input, Output, EventEmitter, OnInit} from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import {Destination} from "../../models/destination";
import {Router} from "@angular/router";
import {DataService} from "../../services/data.service";

@Component({
    moduleId: module.id,
    selector: 'my-destination-search',
    templateUrl: 'destination-search.component.html',
    providers: [DataService]
})
export class DestinationSearchComponent implements OnInit {
    @Input()
    placeModel: Destination;
    @Input()
    label: string;
    @Output()
    placeChangeEvent: EventEmitter<Destination> = new EventEmitter<Destination>();

    private places: Observable<Destination[]>;
    private searchTerms = new Subject<string>();

    constructor(private dataService: DataService,
                private router: Router) {
    }

    ngOnInit(): void {

        this.places = this.searchTerms
            .debounceTime(300)        // wait for 300ms pause in events
            .distinctUntilChanged()   // ignore if next search term is same as previous
            .switchMap(term => term   // switch to new observable each time
                // return the http search observable
                ? this.dataService.searchDestination(term)
                // or the observable of empty heroes if no search term
                : Observable.of<Destination[]>([]));
    }

    search(term: string): void {
        this.searchTerms.next(term);
    }

    selectedDestinationEvent(event: Destination): void {
        console.log()
        this.placeModel = event;
        this.placeChangeEvent.emit(event)
    }


}