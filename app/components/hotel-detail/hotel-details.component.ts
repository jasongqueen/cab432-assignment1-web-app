import {Component, OnInit, EventEmitter, Output} from '@angular/core';
import { Router } from '@angular/router';

import {DataService} from "../../services/data.service";
import {Airport} from "../../models/airport";
import {Hotel} from "../../models/hotel";
import {Photo} from "../../models/photo";

@Component({
  moduleId: module.id,
  selector: 'my-hotel-details',
  templateUrl: 'hotel-details.component.html',
    styleUrls: ['hotel-details.component.css']
})
export class HotelDetailsComponent implements OnInit {

    private hotelPhotos: Photo[];
    private hotel: Hotel;

    constructor(private router: Router, private dataService: DataService) {
    }

    ngOnInit(): void {
        this.hotel = this.dataService.getHotel()
        this.dataService.getHotelPhotos()
          .subscribe(
              res => {
                  this.hotelPhotos = res;
              },
              err => {
                  console.log("Error when retrieving hotel photos");
              }
          );
    }


}
